import pkg from 'ncp';

const { ncp } = pkg;

const ncpEnv = () => ncp(`.env.example`, `.env`);

ncpEnv();

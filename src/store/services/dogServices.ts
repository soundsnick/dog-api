import { api } from "../api";

export const dogServices = {
    getBreeds: () => api.get(`/breeds/list/all`),
    getImages: (payload: string) => (controller: AbortController) => api.get(`/breed/${[payload.split('-').join('/')]}/images/random/3`, { signal: controller.signal }),
};
import { createAtom } from '@reatom/core';
import { AxiosPromise } from 'axios';
import { DefaultState, Statuses, ArgumentTypes } from '../typings';


export const createAtomWithFetch = <T>(name: string, initialState: T, service: (...args: any) => AxiosPromise) => {
    const _initialState: DefaultState<T> = {
        data: initialState,
        status: Statuses.IDLE,
    };

    enum AtomActions {
        SET = 'set',
        FETCH = 'fetch',
    }

    const AtomDependencies = {
        [AtomActions.SET]: (payload: typeof _initialState) => payload,
        [AtomActions.FETCH]: (...args: ArgumentTypes<typeof service>) => args,
    }

    return createAtom(
        AtomDependencies,
        ({ onAction, schedule, create }, state = _initialState) => {

            onAction(AtomActions.SET, (payload) => {
                state = payload;
            });

            onAction(AtomActions.FETCH, (payload) => {
                schedule((dispatch) => {
                    dispatch(create(AtomActions.SET, {
                        ...state,
                        status: Statuses.PENDING,
                    }))

                    service(...payload as [])
                        .then(({ data }) => {
                            dispatch(create(AtomActions.SET, {
                                ...state, 
                                data: data.message,
                                status: Statuses.READY
                            }));
                        })
                        .catch(() => {
                            dispatch(create(AtomActions.SET, {
                                ...state,
                                status: Statuses.REJECTED,
                            }))
                        })
                });
            });

            return state;
        }, name
    );
};
import { createAtom } from "@reatom/core";
import { dogServices } from "../../../services/dogServices";
import { Statuses } from "../../../typings";

const initialState = {
    data: {} as Record<string, ReadonlyArray<string>>,
    status: {} as Record<string, Statuses>,
};

enum AtomActions {
    SET = 'set',
    FETCH = 'fetch',
    ABORT = 'abort',
    RESET = 'reset',
    CANCELED = '_canceled',
    LOADED = '_loaded',
    REJECTED = '_rejected',
}

const AtomDependencies = {
    [AtomActions.SET]: (payload: typeof initialState) => payload,
    [AtomActions.FETCH]: (...args: Parameters<typeof dogServices.getImages>) => args,
    [AtomActions.ABORT]: (name: string) => name,
    [AtomActions.RESET]: (name: string) => name,
    [AtomActions.LOADED]: (name: string, data: ReadonlyArray<string>) => ({ name, data }),
    [AtomActions.REJECTED]: (name: string) => name,
    [AtomActions.CANCELED]: (name: string) => name,
}


export const itemImagesAtom = createAtom(
    AtomDependencies,
    ({ onAction, schedule, create }, state = initialState) => {

        onAction(AtomActions.SET, (payload) => {
            state = payload;
        });

        onAction(AtomActions.LOADED, ({ name, data }) => {
            state = {
                ...state, 
                data: {
                    ...state.data,
                    [name]: data
                },
                status: {
                    ...state.status,
                    [name]: Statuses.READY,
                }
            };
        });

        onAction(AtomActions.REJECTED, (name) => {
            state = {
                ...state,
                status: {
                    ...state.status,
                    [name]: Statuses.REJECTED
                },
            };
        });

        onAction(AtomActions.CANCELED, (name) => {
            state = {
                ...state,
                status: {
                    ...state.status,
                    [name]: state.data[name] ? Statuses.READY : Statuses.IDLE
                }
            };
        });

        onAction(AtomActions.FETCH, (payload) => {
            const name = payload.join('-');

            schedule((dispatch, ctx) => {
                ctx[name] = new AbortController();

                dispatch(create(AtomActions.SET, {
                    ...state,
                    status: {
                        ...state.status,
                        [name]: Statuses.PENDING
                    },
                }))

                dogServices.getImages(...payload)(ctx[name] as AbortController)
                    .then(({ data }) => {
                        dispatch(create(AtomActions.LOADED, name, data.message));
                    })
                    .catch((e) => {
                        if (e.code === 'ERR_CANCELED') {
                            dispatch(create(AtomActions.CANCELED, name));
                        } else {
                            dispatch(create(AtomActions.REJECTED, name));
                        }
                        
                    })
            });
        });

        onAction(AtomActions.ABORT, (name) => {
            schedule((_, ctx) => {
                (ctx[name] as AbortController).abort();
                delete ctx[name];
            });
        });

        onAction(AtomActions.RESET, (name) => {
            schedule((dispatch) => {
                dispatch(create(AtomActions.SET, {
                    ...state,
                    status: {
                        ...state.status,
                        [name]: Statuses.IDLE
                    }
                }));
            });
        });

        return state;
    }, 'itemImagesAtom'
);
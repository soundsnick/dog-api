import { dogServices } from "../../../services/dogServices";
import { createAtomWithFetch } from "../../../_factory/createAtomWithFetch";
import { BreedListModel } from "../types/BreedListModel";

export const breedListAtom = createAtomWithFetch('breedListAtom', {} as BreedListModel, dogServices.getBreeds);

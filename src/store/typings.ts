import { AxiosError } from "axios";

export type ArgumentTypes<F extends Function> = F extends (...args: infer A) => any ? A : never;

export enum Statuses {
    IDLE = 'idle',
    PENDING = 'pending',
    READY = 'ready',
    REJECTED = 'rejected'
};

export type DefaultState<T> = {
    data: T;
    status: Statuses;
    error?: AxiosError;
};
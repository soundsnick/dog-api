import styled from "@emotion/styled";

export const LayoutStyled = styled.div`
    display: flex;
    flex-direction: column;
    height: 100vh;
`;

export const LayoutHeaderStyled = styled.header`
    width: 100%;
`;

export const LayoutContentStyled = styled.div`
    flex: 1;
    overflow: auto;
`;
import { FC } from "react";
import { Props } from "./props";
import { Container } from '../../atoms/container';
import { LayoutContentStyled, LayoutHeaderStyled, LayoutStyled } from "./styled";

export const RootTemplate: FC<Props> = ({ children, ...rest }) => {
    return (
        <LayoutStyled {...rest}>
            <LayoutHeaderStyled>
                <Container>
                    <h1>Dogs</h1>
                </Container>
            </LayoutHeaderStyled>
            <LayoutContentStyled>
                <Container>
                    {children}
                </Container>
            </LayoutContentStyled>
        </LayoutStyled>
    );
}
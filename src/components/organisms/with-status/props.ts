import { ReactNode } from "react"
import { Statuses } from "../../../store/typings";

export type Props = {
    children: ReactNode;
    status: Statuses;
}
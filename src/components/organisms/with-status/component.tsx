import { FC } from "react";
import { Statuses } from "../../../store/typings";
import { Props } from "./props";

export const WithStatus: FC<Props> = ({ children, status }) => {
    return (
        <>
            {status === Statuses.PENDING ? (
                <div>Loading...</div>
            ) : null}
            {status === Statuses.REJECTED ? (
                <div>Error. Try again...</div>
            ) : null}
            {status === Statuses.READY ? children : null}
        </>
    );
};
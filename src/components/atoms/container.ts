import styled from "@emotion/styled";

export const Container = styled.div`
    max-width: 900px;
    position: relative;
    margin: auto;
    width: 100%;
`;
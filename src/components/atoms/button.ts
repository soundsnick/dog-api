import styled from "@emotion/styled";

export const Button = styled.button`
    background: #fff;
    padding: 10px;
    text-align: center;
    border: none;
    border-radius: 10px;
    cursor: pointer;
    transition: opacity 0.2s;
    &:hover {
        opacity: .7;
    }
`;
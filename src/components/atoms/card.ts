import styled from "@emotion/styled";

export const Card = styled.div`
    background-color: #000;
    border-radius: 10px;
    padding: 20px;
    cursor: pointer;
    width: 100%;
`;
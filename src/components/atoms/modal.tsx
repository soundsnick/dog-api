import styled from "@emotion/styled";
import { FC, HTMLAttributes } from "react";
import { createPortal } from "react-dom";

type Props = HTMLAttributes<HTMLDivElement> & {
    show?: boolean;
    onClose?: () => void;
};

const ModalContainerStyled = styled.div`
    position: fixed;
    height: 100vh;
    top: 0;
    left: 0;
    width: 100vw;
    z-index: 9999;
    display: flex;
    align-items: center;
    justify-content: between;
`;

const ModalOverlayStyled = styled.div`
    position: absolute;
    z-index: 99999;
    width: 100%;
    height: 100%;
    background-color: rgba(255, 255, 255,.6);
`;

const ModalContentStyled = styled.div`
    flex: 1;
    position: absolute;
    width: 100%;
    left: 50%;
    transform: translate(-50%);
    background: #000;
    max-width: 1000px;
    z-index: 999999;
    max-height: 90vh;
    overflow: auto;
    border-radius: 10px;
`;

export const Modal: FC<Props> = ({ onClose, children }) => {
    return createPortal(
        <ModalContainerStyled>
            <ModalContentStyled>{children}</ModalContentStyled>
            <ModalOverlayStyled onClick={onClose} />
        </ModalContainerStyled>,
        document.getElementById('root') as HTMLElement
    );
};
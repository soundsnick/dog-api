import {
  Outlet,
  createReactRouter,
  createRouteConfig,
} from '@tanstack/react-router'
import { RootTemplate } from './components/templates/root-template';
import { MainPage } from './features/main';

const routerBase = createRouteConfig({
  component: () => (
    <RootTemplate>
      <Outlet />
    </RootTemplate>
  )
});

const routes = [
    routerBase.createRoute({
      path: '/',
      component: MainPage,
    }),
];

const routeConfig = routerBase.addChildren(routes)

export const router = createReactRouter({ routeConfig })
import { BreedListModel } from "../../store/models/breed/types/BreedListModel"
import { Statuses } from "../../store/typings";

export type Props = {
    data: BreedListModel;
}
import styled from "@emotion/styled";

export const Layout = styled.div`
    display: grid;
    grid-template-columns: repeat(1, auto);
    gap: 10px;
    width: 100%;
`;

export const ImageWrapper = styled.div`
    display: flex;
    gap: 5px;
    width: 100%;
    max-width: 900px;
    overflow: auto;
`;
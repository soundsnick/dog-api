import { FC, Fragment, HTMLAttributes, useMemo } from "react";
import { Card } from "../../components/atoms/card";
import { Props } from "./props";
import { ImageWrapper, Layout } from "./styled";
import { Icon16Picture, Icon24RefreshOutline } from '@vkontakte/icons';
import { Button } from "../../components/atoms/button";
import { useAtom } from "@reatom/react";
import { itemImagesAtom } from "../../store/models/breed/atoms/itemImagesAtom";
import { Statuses } from "../../store/typings";
import { Modal } from "../../components/atoms/modal";

const Item: FC<Omit<HTMLAttributes<HTMLDivElement>, 'children'> & { children: string }> = ({ children, className, ...rest }) => {
    const [{ data, status }, { fetch: fetchImage, abort, reset }] = useAtom(itemImagesAtom); 

    const images = useMemo(() => data[children], [data, children]);
    const itemStatus = useMemo(() => status[children], [status, children]);

    return (
        <Card className={['text-center', className].join(' ')} {...rest}> 
            <div className="d-flex justify-content-between align-items-center">
                <h3>{children}</h3>
                {!itemStatus || itemStatus === Statuses.IDLE ? (
                    <Button onClick={() => fetchImage(children)}>
                        <Icon16Picture />
                    </Button>
                ) : null}
                {itemStatus === Statuses.READY ? (
                    <Button onClick={() => fetchImage(children)}>
                        <Icon24RefreshOutline />
                    </Button>
                ) : null}
                {itemStatus === Statuses.PENDING ? (
                    <Button onClick={() => abort(children)}>
                        Loading...
                    </Button>
                ) : null}
            </div>
            {images?.length > 0 ? (
                <ImageWrapper className={'mt-4'}>
                    {images.map((image) => (
                        <div key={image}>
                            <img src={image} style={{ height: 300 }} />
                        </div>
                    ))}
                </ImageWrapper>
            ) : null}
            {itemStatus === Statuses.REJECTED ? (
                <Modal onClose={() => reset(children)}>
                    <p className="text-center py-4">
                        Oops, something went wrong...
                    </p>
                </Modal>
            ) : null}
        </Card>
    );
}

export const Component: FC<Props> = ({ data }) => {

    return (
        <Layout>
            {Object.entries(data).map(([breed, subBreeds]) => (
                <Fragment key={breed}>
                    <Item key={breed}>{breed}</Item>
                    {subBreeds?.map((subBreed) => (
                        <Item key={`${breed}-${subBreed}`}>{`${breed}-${subBreed}`}</Item>
                    ))}
                </Fragment>
            ))}
        </Layout>
    );
}
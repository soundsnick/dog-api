import { useAtom } from "@reatom/react";
import { useCallback, useEffect } from "react";
import { WithStatus } from "../../components/organisms/with-status";
import { breedListAtom } from "../../store/models/breed/atoms/breedListAtom";
import { Component } from "./component"

export const Container = () => {
    const [{ data, status }, { fetch }] = useAtom(breedListAtom);

    useEffect(() => {
        fetch();
    }, []);

    return (
        <WithStatus status={status}>
            <Component data={data} />
        </WithStatus>
    );
}
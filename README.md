# Getting started
Install dependencies - `$ yarn`
Clone environment files - `$ yarn copy:envs`
Start the project - `$ yarn dev`